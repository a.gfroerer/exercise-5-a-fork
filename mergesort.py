# The old list will be saved in a new list when the old list has been sorted
def assignment(new_list, i, old_list, j):
    new_list[i] = old_list[j]

# merge function
def merge_sort(list_to_sort):

    # The list "list_to_sort" should contain more than one element
    if len(list_to_sort) > 1:

        # Define the middle of the list; Floor division because length of list could be an odd number
        mid = len(list_to_sort) // 2 

        # Define the left and right part of the list and save them sperately to a list
        left = list_to_sort[:mid]
        right = list_to_sort[mid:]

        # Sort the left and right part of the list
        merge_sort(left)
        merge_sort(right)

        # Indices for the left, right and sorted list
        left_index = 0
        right_index = 0
        index_for_sorted = 0

        # To make sure that the left and right index have lengths larger than their assigned index
        while left_index < len(left) and right_index < len(right):

            # If the number in list "left" at position "left_index" is smaller or equal to the number in list "right" at position "right_index"
            if left[left_index] <= right[right_index]:

                # If so, take the number from list "left" at position "left_index" and store it in the new list "list_to_sort" at position "index_for_sorted"
                assignment(list_to_sort, index_for_sorted, left, left_index)

                # Increase "left_index" by one
                left_index += 1

            else:
                # If not (i.e. "left[left_index] > right[right_index]"), take the number from list "right" at position "right_index" and store it in the new list "list_to_sort" at position "index_for_sorted" 
                assignment(list_to_sort, index_for_sorted, right, right_index)

                # Increase "right_index" by one
                right_index += 1

            # Increase "index_for_sorted" by one
            index_for_sorted += 1

        # The next two while-loops deal with the numbers in the list "left" and "right" that haven't been added to the sorted list "list_to_sort". Important if, e.g. list "len(left)" and "len(right)" not the same and then "left_index" or "right_index" larger than "len(left)" or "len(right)"
        while left_index < len(left):
            list_to_sort[index_for_sorted] = left[left_index]
            left_index += 1
            index_for_sorted += 1

        while right_index < len(right):
            list_to_sort[index_for_sorted] = right[right_index]
            right_index += 1
            index_for_sorted += 1
    # else:
    #     print("The list has a length of zero and does not contain any elements that need to be sorted. Please pass a list with a length higher than one.")




import matplotlib.pyplot as plt

my_list = [54, 26, 93, 17, 77, 31, 44, 55, 20]
# Define the x-range
x = range(len(my_list))

# Plot the unsorted list
plt.plot(x, my_list, label="Unsorted list")

# Plot the sorted list
merge_sort(my_list)
plt.plot(x, my_list, label = "Sorted list")

# Legend and axis labels
plt.legend(fancybox=False, fontsize=10, loc=(0.6, 0.87), frameon=False)
plt.xlabel("Position of List Numbers in the List")
plt.ylabel("List Numbers")

# Show the plot
plt.show()